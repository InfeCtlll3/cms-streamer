_**Cisco Meeting Server CoSpace Streamer Generator**_

This script was made to aid UC engineers to enable and set streaming URL on CMS spaces.
Note that streaming has to be already enabled globally, as this script only assigns a "streamURL" tag with the
proper URL in the following format:

`rtmp://broadcast:broadcast@[VBRICK IP|FQDN]/live/[CALL_ID]`

Note: VBrick default configuration. For other streaming services, please edit the python source file accordingly to
match username/password for the streaming URL.

**Usage**:
Run without quotes:

'python csg.py -u [api_username] -p [api_password] -s [CMS_server_addr] -b [VBrick_DME_addr]'

on your command line interface.


**Roadmap**:
* Configure Streaming globally
* GUI


Please fell free to reach me out at **`aocbrasil[at]gmail[dot]com`** for any doubts or any usage suggestions and queries.


