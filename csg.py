'''

Written by Andre Castro, 2018
CCIE #42374
aocbrasil[at]gmail[dot]com

gitlab.com/aocbrasil

'''

from lxml import etree

import argparse
import sys
import requests
import urllib3


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = MyParser()
parser.add_argument('-u', nargs=1, required=True, help="API user username")
parser.add_argument('-p', nargs=1, required=True, help="API user password")
parser.add_argument('-s', nargs=1, required=True, help="Server ip/fqdn")
parser.add_argument('-b', nargs=1, required=True, help="Broadcast server")
args = parser.parse_args()

apiuser = args.u[0]
apipass = args.p[0]
url = 'https://' + args.s[0] + ':445/api/v1/cospaces/'
bcastsrv = args.b[0]

xml = requests.get(url, auth=(apiuser, apipass), verify=False)

doc = etree.XML(xml.text)

nodes = doc.xpath('/coSpaces/coSpace')

for node in nodes:
    print(url + node.attrib['id'])
    uri = node.getchildren()[2].text
    print('Configuring room...  ' + uri + '...')
    r = requests.put(url + node.attrib['id'], auth=(apiuser, apipass), data={'streamUrl': 'rtmp://broadcast:broadcast@' + bcastsrv + '/live/' + uri}, verify=False)
    print(r.status_code)

